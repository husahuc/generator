<div align="center">
  <a href="https://husahuc.com/en/projets/generataure/">
    <img src="https://gitlab.com/husahuc/generator/raw/master/Generataure.png"
     alt="Generataure">
  </a>
</div>

# Generator
Projet en VueJS pour faire un generateur de mot, a partir de sylabes.

# Objectif
Gestion de la database en Firebase, et en utilisant vuex.
Front fait avec Buefy (framework vue qui est basee sur Bulma)
