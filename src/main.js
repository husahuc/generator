import Vue from 'vue'
import App from './App.vue'
import { store } from './store'
import router from "./router"
const fb = require("./config/firebase.js")
import "@/plugins/buefy.js";
Vue.config.productionTip = false
import titleMixin from './Mixin/title'
Vue.mixin(titleMixin)

let app
fb.auth.onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App),
    })
  }
})