import firebase from 'firebase'
import 'firebase/firestore'

var firebaseConfig = {
  apiKey: "AIzaSyCK5xwoWsTxMCSiBkyMvlfgJNLebuTBdSU",
  authDomain: "generataure.firebaseapp.com",
  databaseURL: "https://generataure.firebaseio.com",
  projectId: "generataure",
  storageBucket: "generataure.appspot.com",
  messagingSenderId: "758798468362",
  appId: "1:758798468362:web:7071a67a4d3e4bdaeca6f3",
  measurementId: "G-5X5VB49R62"
};
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

const usersCollection = db.collection('users')
const sylabesCollection = db.collection('sylabes')
const stylesCollection = db.collection('styles')
//const typesCollection = db.collection('types')
const associationsCollection = db.collection('association')
const generesCollection = db.collection('generes')
const motsCollection = db.collection('mots')
const LanguesCollection = db.collection('Langues')

export {
  db,
  auth,
  currentUser,
  usersCollection,
  sylabesCollection,
  stylesCollection,
  associationsCollection,
  generesCollection,
  motsCollection,
  LanguesCollection
}