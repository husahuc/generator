import Vue from "vue";
import Vuex from "vuex";

const fb = require("../config/firebase.js");

fb.auth.onAuthStateChanged((user) => {
  if (user) {
    store.commit("setCurrentUser", user);
    store.dispatch("fetchUserProfile");

    fb.usersCollection.doc(user.uid).onSnapshot((doc) => {
      store.commit("setUserProfile", doc.data());
    });

    fb.sylabesCollection.where('user', '==', user.uid).onSnapshot((querySnapshot) => {
      let sylabesArray = [];

      querySnapshot.forEach((doc) => {
        let sylabe = doc.data();
        if (doc.id) {
          sylabe.id = doc.id;
          sylabesArray.push(sylabe);
        }
      });

      store.commit("setSylabes", sylabesArray);
    });

    fb.stylesCollection.where('user', '==', user.uid).onSnapshot((querySnapshot) => {
      let styleArray = [];

      querySnapshot.forEach((doc) => {
        let style = doc.data();

        if (doc.id) {
          style.id = doc.id;
          styleArray.push(style);
        }
      });

      store.commit("setStyles", styleArray);
    });

    fb.associationsCollection.where('user', '==', user.uid).onSnapshot((querySnapshot) => {
      let associationArray = [];

      querySnapshot.forEach((doc) => {
        let association = doc.data();
        if (doc.id) {
          association.id = doc.id;
          associationArray.push(association);
        }
      });

      store.commit("setAssociations", associationArray);
    });

    fb.motsCollection.where('user', '==', user.uid).onSnapshot((querySnapshot) => {
      let motArray = [];

      querySnapshot.forEach((doc) => {
        let mot = doc.data();
        if (doc.id) {
          mot.id = doc.id;
          motArray.push(mot);
        }
      });

      store.commit("setMots", motArray);
    });
  }
});

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    sylabes: [],
    styles: [],
    types: [
      { name: "Nom de personage", attr: "perso", id: 1 },
      { name: "Nom de ville", attr: "ville", id: 2 },
    ],
    associations: [],
    generes: [],
    mots: [],
    langues: [],

    currentUser: null,
    userProfile: {},

    introduction: false,
  },

  getters: {
    GetAllAssocSylabe: (state) => (sylabe) => {
      return state.associations.filter((assos) =>
        assos.sylabes.find((elem) => elem == sylabe.name)
      );
    },
    GetUser: (state) => {
      return state.currentUser;
    },
  },

  mutations: {
    EmptySylabe(state) {
      state.sylabes = [];
    },
    setSylabes(state, val) {
      if (val) {
        state.sylabes = val;
      } else {
        state.sylabes = [];
      }
    },
    setSylabe(state, val) {
      if (val) {
        state.sylabes.push(val);
      }
    },

    setStyles(state, val) {
      if (val) {
        state.styles = val;
      } else {
        state.styles = [];
      }
    },
    setStyle(state, val) {
      if (val) {
        state.styles.push(val);
      }
    },

    setAssociations(state, val) {
      if (val) {
        state.associations = val;
      } else {
        state.associations = [];
      }
    },
    setAssociation(state, val) {
      if (val) {
        state.associations.push(val);
      }
    },

    setMots(state, val) {
      if (val) {
        state.mots = val;
      } else {
        state.mots = [];
      }
    },

    AddGenerated(state, generated) {
      state.generes.unshift(generated);
      if (state.generes.length > 5) state.generes.pop();
    },

    setCurrentUser(state, val) {
      state.currentUser = val;
    },
    setUserProfile(state, val) {
      state.userProfile = val;
    },

    setIntroduction(state, val) {
      state.introduction = val;
    },
  },

  actions: {
    AddGenerated({ commit }, generated) {
      commit("AddGenerated", generated);
    },

    fetchUserProfile({ commit, state }) {
      fb.usersCollection
        .doc(state.currentUser.uid)
        .get()
        .then((res) => {
          commit("setUserProfile", res.data());
        })
        .catch((err) => {
          console.log(err);
        });
    },
    clearData({ commit }) {
      commit("setCurrentUser", null);
      commit("setUserProfile", {});
    },

    setIntroduction(commit, val) {
      commit("setIntroduction", val);
    },
  },
});
