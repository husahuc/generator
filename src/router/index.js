import Vue from "vue";
import VueRouter from "vue-router";
//import store from "../store/index";
import firebase from "firebase";
import Generate from "../views/generate.vue";
import User from "../views/user";
import Root from "../views/root.vue";
import Introduction from "../views/introduction.vue";
import Profile from "../views/profile"

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: User,
  },
  {
    path: "/",
    name: "Generataure",
    component: Root,
    children: [
      {
        path: "",
        name: "Nome",
        redirect: "Generate"
      },
      {
        path: "generate",
        name: "Generate",
        component: Generate,
        meta: {
          requireAuth: true
        }
      },
      {
        path: "introduction",
        name: "Introduction",
        component: Introduction,
        meta: {
          requireAuth: true
        }
      },

      {
        path: "profile/:userid",
        name: "profile",
        props: route => ({
          userid: String(route.params.userid)
        }),
        component: Profile,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  const requireAuth = to.matched.some((x) => x.meta.requireAuth);
  const currentUser = firebase.auth().currentUser;
  
  if (requireAuth && !currentUser) {
    next("login");
  } else if (requireAuth && currentUser) {
    next()
  } else {
    next()
  }
});

export default router;
